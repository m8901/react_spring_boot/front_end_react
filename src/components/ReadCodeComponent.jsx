import React, { Component } from 'react';
import CodeService from '../service/CodeService';

class ReadCodeComponent extends Component {
    constructor(props) {
        super(props);

        this.state = { 
            CODE_ID: this.props.match.params.CODE_ID,
            code: {}
        }

        this.goToUpdate = this.goToUpdate.bind(this);

    }

    componentDidMount() {
        CodeService.getOneCode(this.state).then( res => {
            this.setState({code: res.data.data});
        });
    }

    returnDate(cTime, uTime) {
        return (
            <div className = "row">
                <label>순서 : [ {cTime} ] / 추가 값 : [ {uTime} ] </label>
            </div>
        )
    }

    returnCodeType(typeNo) {
        let type = null;
        if (typeNo === "Y") {
            type = "사용";
        } else {
            type = "미사용";
        }

        return (
            <div className = "row">
                <label> 사용여부 : </label> {type}
            </div>
        )

    }

    goToList() {
        this.props.history.push('/code');
    }

    goToUpdate = (event) => {
        event.preventDefault();
        this.props.history.push(`/code-create/${this.state.CODE_ID}`);
    }

    deleteView = async function () { 
        if(window.confirm("정말로 삭제하시겠습니까?\n복구 할 수 없습니다!!")) {
            CodeService.deleteCode(this.state).then( res => {
                console.log("delete result => "+ JSON.stringify(res));
                if (res.status === 200) {
                    this.props.history.push('/code');
                } else {
                    alert("삭제가 실패했습니다.");
                }
            });

        }
    }

    render() {
        return (
            <div>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className ="text-center"> Read Detail</h3>
                    <div className = "card-body">
                            <div className = "row">      
                                <label> 코드 ID </label> : {this.state.code.CODE_ID}
                            </div>

                            <div className = "row">
                                <label> 코드 명 </label> : <br></br>
                                <input type="text" value={this.state.code.CODE_NAME} readOnly/> 
                            </div >

                            <div className = "row">
                                <label> PARENT ID  </label>: 
                                <textarea value={this.state.code.PARENT_ID} readOnly/> 
                            </div>
                            {this.returnDate(this.state.code.DISPLAY_SEQ, this.state.code.TEMP_01) }
                            {this.returnCodeType(this.state.code.USE_YN)} 
                            <button className="btn btn-primary" onClick={this.goToList.bind(this)} style={{marginLeft:"10px"}}>목록으로 이동</button>
                            <button className="btn btn-info" onClick={this.goToUpdate} style={{marginLeft:"10px"}}>수정</button>
                            <button className="btn btn-danger" onClick={() => this.deleteView()} style={{marginLeft:"10px"}}>삭제</button>
                    

                    </div>
                </div>

            </div>
        );
    }
}

export default ReadCodeComponent;