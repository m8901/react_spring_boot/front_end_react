import React, { Component } from 'react';
import CodeService from '../service/CodeService';

class ListBoardComponent extends Component {
    constructor(props) {
        super(props)
        this.state = { 
            codes: []
        }

        this.createCode = this.createCode.bind(this);
		
    }
    componentDidMount() {
        CodeService.getCodes().then((res) => {
            this.setState({ codes: res.data.data.result});
        });
    }

    createCode() {
        this.props.history.push('/code-create/');
    }

    readCode(CODE_ID) {
        this.props.history.push(`/code-read/${CODE_ID}`);
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Code List</h2>

                <div className = "row">
                    <button className="btn btn-primary" onClick={this.createCode}>작성</button>
                </div>

                <div className ="row">
                    <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>CODE ID</th>
                                <th>CODE 명 </th>
                                <th>PARENT ID</th>
                                <th>순서 </th>
                                <th>추가 값 </th>
                                <th>사용여부 </th>
                                <th>작성일</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.codes.map(
                                    code => 
                                    <tr key = {code.CODE_ID}>
                                        <td> {code.CODE_ID} </td>
                                        <td  onClick = {() => this.readCode(code.CODE_ID)}>{code.CODE_NAME} </td> 
                                        <td> {code.PARENT_ID} </td>  
                                        <td> {code.DISPLAY_SEQ} </td>        
                                        <td> {code.TEMP_01} </td>                            
                                        <td> {code.USE_YN} </td>
                                        <td> {code.REG_DATE} </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default ListBoardComponent;