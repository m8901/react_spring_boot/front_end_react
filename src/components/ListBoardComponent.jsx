import React, { Component } from 'react';
import BoardService from '../service/BoardService';

class ListBoardComponent extends Component {
    constructor(props) {
        super(props)
    // # 1. 
        this.state = { 
            boards: []
        }
		
    }
    // # 2. 
    componentDidMount() {
        BoardService.getBoards().then((res) => {
            this.setState({ boards: res.data});

            console.log("res.data.data:"+res.data);

        });
    }

    // # 3.
    render() {
        return (
            <div>
                <h2 className="text-center">Boards List</h2>
                <div className ="row">
                    <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>글 번호</th>
                                <th>타이틀 </th>
                                <th>내용</th>
                                <th>작성자 </th>
                                <th>작성일 </th>
                                <th>갱신일 </th>
                                <th>조회수</th>
                            </tr>
                        </thead>
                        <tbody>
                            {

                                
                                this.state.boards.map(
                                    board => 
                                    <tr key = {board.BOARD_SEQ}>
                                        <td> {board.BOARD_SEQ} </td>
                                        <td> {board.TITLE} </td>
                                        <td> {board.CONTENTS} </td>
                                        <td> {board.REG_ID} </td>
                                        <td> {board.REG_DATE} </td>
                                        <td> {board.MOD_DATE} </td>
                                        <td> {board.HIT_CNT} </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default ListBoardComponent;