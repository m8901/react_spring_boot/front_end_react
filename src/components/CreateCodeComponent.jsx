import React, { Component } from 'react';
import CodeService from '../service/CodeService';

class CreateCodeComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            CODE_ID: this.props.match.params.CODE_ID,
            type: 'Y',
            title: '',
            contents: '',
            memberNo: ''
        }

        this.changeTypeHandler = this.changeTypeHandler.bind(this);
        this.changeTitleHandler = this.changeTitleHandler.bind(this);
        this.changeContentsHandler = this.changeContentsHandler.bind(this);
        this.changeMemberNoHandler = this.changeMemberNoHandler.bind(this);
        this.createCode = this.createCode.bind(this);
    }

    changeTypeHandler = (event) => {
        this.setState({type: event.target.value});
    }
    changeTitleHandler = (event) => {
        this.setState({title: event.target.value});
    }
    changeContentsHandler = (event) => {
        this.setState({contents: event.target.value});
    }
    changeMemberNoHandler = (event) => {
        this.setState({memberNo: event.target.value});
    }

    createCode = (event) => {
        event.preventDefault();

        let param = {
            USE_YN: this.state.type,
            CODE_ID: this.state.title,
            CODE_NAME: this.state.contents,
            PARENT_ID: this.state.memberNo
        };
        // let param ={};

        // param.JsonData = JSON.stringify({
        //     userId: userId.current.getValue(),
        //   });

        // param.USE_YN = this.state.type;
        // param.CODE_ID = this.state.title;
        // param.CODE_NAME = this.state.contents;
        // param.PARENT_ID = this.state.memberNo;
       
        CodeService.createCode(param).then(res => {
            this.props.history.push('/code');
        });
    }

    cancel() {
        this.props.history.push('/code');
    }

    getTitle() {
        // if (this.state.no === '_create') {
            return <h3 className="text-center">새글을 작성해주세요</h3>
        // } else {
        //     return <h3 className="text-center">{this.state.no}글을 수정 합니다.</h3>
        // }
    }

    render() {
        return (
            <div>
                <div className = "container">
                    <div className = "row">
                        <div className = "card col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">코드 관리</h3>
                            <div className = "card-body">
                                <form>
                                    <div className = "form-group">
                                        <label> CODE_ID </label>
                                        <input type="text" placeholder="CODE_ID" name="title" className="form-control" 
                                        value={this.state.title} onChange={this.changeTitleHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> CODE_NAME  </label>
                                        <textarea placeholder="CODE_NAME" name="contents" className="form-control" 
                                        value={this.state.contents} onChange={this.changeContentsHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> PARENT_ID  </label>
                                        <input placeholder="PARENT_ID" name="memberNo" className="form-control" 
                                        value={this.state.memberNo} onChange={this.changeMemberNoHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> USE_YN </label>
                                        <select placeholder="USE_YN" name="type" className="form-control" 
                                        value={this.state.type} onChange={this.changeTypeHandler}>
                                            <option value="Y" checked>사용</option>
                                            <option value="N">미사용</option>
                                        </select>
                                    </div>
                                    <button className="btn btn-success" onClick={this.createCode}>Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft:"10px"}}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default CreateCodeComponent;