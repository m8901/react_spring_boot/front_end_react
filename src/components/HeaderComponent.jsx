import React, { Component } from 'react';

class HeaderComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }
    render() {
        return (
            <div>
                <header>
                    <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                        <div><a href="http://localhost:3000" className="navbar-brand"> Home</a></div>
                        <div><a href="http://localhost:3000/board" className="navbar-brand"> BOARD</a></div>
                        <div><a href="http://localhost:8080/oms/main" className="navbar-brand"> 총무지원시스템</a></div>
                        <div><a href="http://localhost:8080/bbs/main" className="navbar-brand"> 행동기반안전시스템</a></div>
                    </nav>
                </header>
            </div>
        );
    }
}

export default HeaderComponent;