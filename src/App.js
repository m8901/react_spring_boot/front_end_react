
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'; 
import ListBoardComponent from './components/ListBoardComponent';
import ListCodeComponent from './components/ListCodeComponent';
import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';
import CreateCodeComponent from './components/CreateCodeComponent';
import ReadCodeComponent from './components/ReadCodeComponent';

function App() {
  return (
    <div> 
      <Router>            
        <HeaderComponent/>
          <div className="container">
            <Switch> 
              <Route path = "/" exact component = {ListCodeComponent}></Route>
              <Route path = "/board" component = {ListBoardComponent}></Route>
              <Route path = "/code" component = {ListCodeComponent}></Route>
              <Route path = "/code-create" component = {CreateCodeComponent}></Route>
              <Route path = "/code-read/:CODE_ID" component = {ReadCodeComponent}></Route>
            </Switch>
          </div>
        <FooterComponent/>
      </Router>
    </div>
  );
}

export default App;