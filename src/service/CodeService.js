import axios from 'axios'; 

const CODE_API_BASE_URL = "http://localhost:8080/api/sys/code"; 

const axiosRequestConfig = {
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
    },
    responseType: 'json',
};

class CodeService {

    getCodes() {
        return axios.get(CODE_API_BASE_URL+"/code-list");
    }

    createCode(param) {        
        return axios.post(CODE_API_BASE_URL+"/code-create", JSON.stringify(param), axiosRequestConfig);
    }

    getOneCode(param) {    
        return axios.post(CODE_API_BASE_URL+"/code-read", JSON.stringify(param), axiosRequestConfig);
    }

    deleteCode(param) {
        return axios.post(CODE_API_BASE_URL+"/code-delete", JSON.stringify(param), axiosRequestConfig);
    }

}

export default new CodeService();